#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
# mingw does not know about the arm64 specific cflag branch-protection
# so strip it out of the build, so that the build doesn't fail.
export DEB_CFLAGS_MAINT_STRIP = -mbranch-protection=standard

ifeq ($(shell dpkg-vendor --derives-from Ubuntu && echo yes),yes)
	VENDOR_CONFIGURE_OPTIONS = --with-libssh2=included
endif
ifeq ($(shell dpkg-vendor --derives-from kali && echo yes),yes)
	VENDOR_CONFIGURE_OPTIONS = --with-libpcap=included
endif

%:
	dh $@

override_dh_auto_clean:
	dh_auto_clean
	dh_auto_clean --sourcedir=ndiff --buildsystem=pybuild
	dh_auto_clean --sourcedir=zenmap --buildsystem=pybuild

override_dh_autoreconf:
	cp -r debian/libpcap .
	dh_autoreconf

override_dh_auto_configure:
	dh_auto_configure -- \
		--with-liblua=/usr \
		--with-liblinear \
		--without-zenmap \
		--without-ndiff \
		--enable-ipv6 \
		STRIP=/bin/true

execute_after_dh_auto_configure-indep:
	dh_auto_configure --sourcedir=ndiff --buildsystem=pybuild
	dh_auto_configure --sourcedir=zenmap --buildsystem=pybuild

mingw_CFLAGS = $(filter-out -mbranch-protection=standard,$(CFLAGS))

execute_after_dh_auto_build-indep:
	# Work around pybuild refusing to build from more than one sourcedir
	# 
	# I: pybuild plugin_pyproject:144: Unpacking wheel built for
	# python3.12 with "installer" module
	# W: pybuild plugin_pyproject:150: Scripts directory already
	# exists, skipping unpack. Is the Python package being built
	# twice?
	dh_auto_build --sourcedir=ndiff --buildsystem=pybuild
	rm -rf .pybuild-ndiff && mv .pybuild .pybuild-ndiff
	dh_auto_build --sourcedir=zenmap --buildsystem=pybuild
	rm -rf .pybuild-zenmap && mv .pybuild .pybuild-zenmap
	mkdir -p .pybuild && cp -al .pybuild-*/* .pybuild && rm -rf .pybuild-*

	cd nselib/data/jdwp-class && /usr/lib/jvm/default-java/bin/javac *.java
	cd nselib/data/psexec && \
		i686-w64-mingw32-gcc ${CPPFLAGS} ${mingw_CFLAGS} -o nmap_service.exe nmap_service.c && \
		gzip -c -n9 nmap_service.exe | base64 | tac > nmap_service.ex_

override_dh_auto_test-arch:

override_dh_auto_test-indep:
	dh_auto_test --sourcedir=ndiff --buildsystem=pybuild
	dh_auto_test --sourcedir=zenmap --buildsystem=pybuild

execute_after_dh_auto_install-arch:
	mv debian/tmp/usr/share/man/pt_PT debian/tmp/usr/share/man/pt

execute_after_dh_auto_install-indep:
	dh_auto_install --sourcedir=ndiff --buildsystem=pybuild
	dh_auto_install --sourcedir=zenmap --buildsystem=pybuild
